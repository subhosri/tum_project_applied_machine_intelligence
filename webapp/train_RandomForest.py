def train_RandomForest(train_csv, label_csv):
    """

    Description: Used to retrain the rf model

    Arguments:
    train_csv -- where the train CSV features are saved
    label_csv -- where the annotations CSV are saved
    """
    import numpy as np
    import pickle
    from sklearn.ensemble import RandomForestClassifier

    # Load training data X, y
    features_merged_train = np.loadtxt(train_csv, delimiter=",")
    labels_train = np.loadtxt(label_csv, delimiter=",")

    # Train/fit decision tree
    clf = RandomForestClassifier(
        n_estimators=500, max_depth=15, max_features=11, bootstrap=True, random_state=18
    ).fit(features_merged_train, labels_train)

    # Save using pickle
    with open("model_RF_new.pkl", "wb") as f:
        pickle.dump(clf, f)
