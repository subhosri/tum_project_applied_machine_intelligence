"""

This skript defines our model
resnet18+some linear layers

"""
import os
import random
from PIL import Image
import torch
import torch.nn as nn
from torchvision import transforms


# function to resize image
preprocess = transforms.Compose(
    [
        # transforms.Resize((3,224,224)),
        transforms.ToTensor(),
    ]
)

pred = []


def test(model):
    for i in range(10):
        random_img = random.choice(images)  # select an image randomly
        img = Image.open(random_img)
        input_tensor = preprocess(img)  # resize image
        input_batch = input_tensor.unsqueeze(0)
        output = model(input_batch)
        pred.append(output)
    return pred


class net(nn.Module):
    def __init__(self, in_dim, n_hidden_1, n_hidden_2, n_hidden_3, out_dim):
        super().__init__()
        self.layer1 = nn.Sequential(nn.Linear(in_dim, n_hidden_1))
        self.layer2 = nn.Sequential(nn.Linear(n_hidden_1, n_hidden_2))
        self.layer3 = nn.Sequential(nn.Linear(n_hidden_2, n_hidden_3))
        self.layer4 = nn.Sequential(nn.Linear(n_hidden_3, out_dim))

    def forward(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        return x


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# print(device)
extralayers = net(1000, 200, 50, 10, 4)
resnet = torch.hub.load("pytorch/vision:v0.10.0", "resnet18", pretrained=True)
# resnet.eval()
# model=nn.Sequential(resnet, extralayers,nn.Softmax(dim=1))
model = nn.Sequential(resnet, extralayers)
# model=nn.Sequential(resnet, extralayers)
model.to(device)

if __name__ == "__main__":
    path = "C:/Users/diony/Downloads/G47"

    # acquire paths to all images
    images = []
    for folder in os.listdir(path):
        for image in os.listdir(path + "/" + folder):
            images.append(os.path.join(path, folder, image))

    pred = test(model)
    print(pred)
