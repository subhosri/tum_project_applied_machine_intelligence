import json
import os
import numpy as np
import cv2 
from flask import Flask, Response, redirect, render_template, request, url_for, flash, jsonify
from werkzeug.utils import secure_filename
from forms import LabelForm, PhotoForm
from model import model
import torch
from torchvision import transforms


# Create Flask App
app = Flask(__name__)
app.config["SECRET_KEY"] = "sucess2G15"
app.config["UPLOAD_FOLDER"] = "static/files"

# ------------ Route to home page --------------------


@app.route("/", methods=["GET", "POST"])
def home():
    return render_template("home.html")

# ------------ Route to labeling page --------------------



@app.route("/labeling", methods=["GET", "POST"])
def upload_images():
    # Load form class instance from forms.py
    form1 = PhotoForm()
    # Validate image files after submit
    if form1.validate_on_submit():
        uploaded_files = form1.photos.data
        # Error handling for too many images
        if len(uploaded_files) > 50:
            error = "You are only allowed to upload a maximum of 50 files!"
            return render_template("labeling.html", form1=form1, error=error)
        else:
            for file in form1.photos.data:
                filename = secure_filename(file.filename)
                # Save image in the static folder
                file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            # Flash this message (if successful)
            flash("Images succesfully uploaded")
            # Get image names to plot grid
            uploaded_images = os.listdir(app.config["UPLOAD_FOLDER"])
            return render_template("labeling.html", uploaded_images=uploaded_images)
            # redirect(url_for("view_label_images"))

    # Remove all files if "Labeling" page is clicked
    filelist = [f for f in os.listdir(app.config["UPLOAD_FOLDER"])]
    for f in filelist:
        os.remove(os.path.join(app.config["UPLOAD_FOLDER"], f))
    return render_template("labeling.html", form1=form1)


# View and label images route
@app.route("/labeling/start", methods=["GET", "POST"])
def view_label_images():
    # Load form class instance from forms.py
    form2 = LabelForm(request.form)
    labels = {"annotations": []}
    # Get the annotations list
    exports = labels["annotations"]
    # Get file paths list
    image_names = os.listdir(app.config["UPLOAD_FOLDER"])
    # Check if the export button is clicked
    if request.method == "POST":
        for i, curr_file_name in enumerate(image_names):
            # Get data from the label form
            label = form2.labels[i].data
            # Append export of each labeled image
            export = {
                "file_name": curr_file_name,
                "damage": label,
            }
            exports.append(export)
            # Clean labeled image from static folder
            os.remove(os.path.join(
                app.config["UPLOAD_FOLDER"], curr_file_name))
        # Download json file after all labels are filled
        return Response(
            json.dumps(labels),
            mimetype="application/json",
            headers={"Content-Disposition": "attachment;filename=labels.json"},
        )

    return render_template(
        "labeling.html",
        form2=form2,
        image_names=image_names,
    )


# ------------ Route to prediction page --------------------
# Upload images route
@app.route("/prediction", methods=["GET", "POST"])
def upload_image_pred():
    # Load form class instance from forms.py
    form1 = PhotoForm()
    # Validate image files after submit
    if form1.validate_on_submit():
        # Get uploaded files
        uploaded_files = form1.photos.data
        # Error handling for too many images
        if len(uploaded_files) > 50:
            error = "You are only allowed to upload a maximum of 50 files!"
            return render_template("prediction.html", form1=form1, error=error)
        else:
            # Save all images into static folder
            for file in form1.photos.data:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            # Flash this message (if successful)
            flash("Images succesfully uploaded")
            # Get file paths list
            grid_images = os.listdir(app.config["UPLOAD_FOLDER"])
            return render_template("prediction.html", grid_images=grid_images)

    # Remove all files if "Labeling" page is clicked
    filelist = [f for f in os.listdir(app.config["UPLOAD_FOLDER"])]
    for f in filelist:
        os.remove(os.path.join(app.config["UPLOAD_FOLDER"], f))
    return render_template("prediction.html", form1=form1)


# View predictions route
@app.route("/prediction/show", methods=["GET", "POST"])
def image_predictions():
    pred_images = os.listdir(app.config["UPLOAD_FOLDER"])

    # Read images and convert to numpy
    predictions = []
    prob_dicts = []
    for name in pred_images:
        # Read image and convert to numpy
        img = cv2.imread(app.config["UPLOAD_FOLDER"] + "/" + name)

        """
        # Preprocessing
        mean = [0.3518, 0.3335, 0.3398]
        std = [0.2715, 0.2676, 0.2685]
        preprocess = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Resize((224, 224)),
                transforms.Normalize(mean, std),
            ]
        )
        torch_image = preprocess(img).reshape([1, 3, 224, 224])
        """

        # Convert and reshape to numpy array
        np_image = np.array(img).reshape([1, 3, 224, 224])

        # Convert to torch tensor
        torch_image = torch.from_numpy(np_image)

        # Get raw prediction
        with torch.no_grad():
            predicted_output_raw = model(torch_image.float())

        # Apply softmax and get probabilities
        softmax = torch.nn.Softmax(dim=1)
        predicted_output = softmax(predicted_output_raw)
        probability = torch.round(predicted_output, decimals=2)

        # Get max index of one-hot
        _, predicted_max_idx = torch.max(probability, 1)

        # Convert the indexes to label names
        if predicted_max_idx == 0:
            prediction = "other"
        elif predicted_max_idx == 1:
            prediction = "rim"
        elif predicted_max_idx == 2:
            prediction = "scratch"
        elif predicted_max_idx == 3:
            prediction = "dent"

        # Get probability dictionairy
        prob_dict = {"dent": float("{:.2f}".format(probability[:, 3].item())),
                     "scratch": float("{:.2f}".format(probability[:, 2].item())),
                     "rim": float("{:.2f}".format(probability[:, 1].item())),
                     "other": float("{:.2f}".format(probability[:, 0].item())),
                     }

        # Add to predcition list
        predictions.append(prediction)
        prob_dicts.append(prob_dict)

    return render_template("prediction.html", pred_images=pred_images, predictions=predictions, prob_dicts=prob_dicts)


# ------------ Run flask app ------------------
if __name__ == "__main__":
    app.run(debug=True)
