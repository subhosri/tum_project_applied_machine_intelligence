# Welcome to the backend repo!

## Templates folder

Description: Contains all necessary html files for the frontend respresentation.

## Static folder

Description: Contains all static files for the frontend respresentation and is used as cache for uploading files from the user interface. All files are automatically deleted after used.

## forms.py

Description: Contains all input fields and forms for the frontend respresentation

## main.py

Description: Connects the rest of the files and runs the web app

## main_with_db.py

Description: Contains outdated code but has a database connection (not needed for labeling but only for prediction). Its needed to transfer the functionality to the prediction page.

## init_database.py

Description: Initializes database connection and uploads the labele dataset to the datatabase. It will be useful for retaining afterwards.
