import json
import os
from flask_sqlalchemy import SQLAlchemy
from main import app
from PIL import Image
from io import BytesIO

db = SQLAlchemy(app)

class Image5(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    img = db.Column(db.LargeBinary, nullable = True)
    name = db.Column(db.Text, nullable = True, unique=True)
    label = db.Column(db.Text, nullable = True)


db.create_all()


json_file = open("C:\\Users\\basu\\Desktop\\AIM\\Group15\\annotations.json", "r", encoding="utf-8")
json_data = json.load(json_file)
a = 0

dir_path = "C:\\Users\\basu\\Desktop\\AIM\\Group15\\Cropped Images"

# for i in json_data['annotations']:
#     print(i['filename'])
#     a +=1
# print(a)




for i in json_data['annotations']:

    if i['filename'] in os.listdir(dir_path):

        label = i['label']
        image = dir_path + '\\' + "{}".format(i['filename'])
        image = Image.open(image)
        buf = BytesIO()
        # Save the image as jpeg to the buffer
        image.save(buf, 'jpeg')

        # Rewind the buffer's file pointer
        buf.seek(0)

        # Read the bytes from the buffer
        image_bytes = buf.read()

        # Close the buffer
        buf.close()


        image = Image5(img=image_bytes, name=i['filename'], label=label)
        db.session.add(image)
        db.session.commit()






