import torch
import torchvision
import torchvision.transforms as T
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
import json
import cv2
import os.path
import splitfolders
import numpy as np

#load company annotated json file
json_file = open("C:/Users/Basu/Documents/AMI/Data/Annotations/annotated_functional_test3_fixed.json", "r", encoding="utf-8")
json_data = json.load(json_file)

#Path with the original images
abs_path = "C:/Users/Basu/Documents/AMI/Data/Images"
#Path for the resized cropped and augmented images
#save_path = "C:/Users/Basu/Documents/AMI/Aug_crop_resized_images"
save_path = 'C:/Users/Basu/Documents/input_folder/Aug_crop_resized_images'
#Path for the normalised test images
save_path_norm_test = "C:/Users/Basu/Documents/AMI/norm_test"
#Path for the normalised train images
save_path_norm_train = "C:/Users/Basu/Documents/AMI/norm_train"
#Path for the normalised validation images
save_path_norm_val = "C:/Users/Basu/Documents/AMI/norm_val"


transform_to_tensor = T.ToTensor()
transform_PIL = T.ToPILImage()
resized_transform = T.Resize((224,224))

idx = 1 #Image Index
for i in json_data['annotations']:
     for j in json_data["images"]:
        if i["image_id"] == j["id"]:
            
            #Image Source
            source = os.path.join(abs_path, j['file_name'])

            
                
                #Load Image
            img = cv2.imread(source)

                #Box coordinats
            x_min = i['bbox'][0]
            y_min = i['bbox'][1]
            x_max = i['bbox'][0] + i['bbox'][2]
            y_max = i['bbox'][1] + i['bbox'][3]

                #Image Crop
            transform_PIL = T.ToPILImage()
            crop_tensor = img[y_min:y_max, x_min:x_max]
            cropped_img = transform_PIL(crop_tensor)
            
             #image augumentation
            random_degree = np.random.randint(1,360)
            aug_transform = transforms.Compose([transforms.RandomHorizontalFlip(),transforms.RandomRotation(random_degree),transforms.ToTensor()])
            augumented_tensor = aug_transform(cropped_img)
            augumented_img = transform_PIL(augumented_tensor)
            
            
               #resize Images (crop and augumented)
            resized_img_aug = resized_transform(augumented_img)
            resized_img_crop = resized_transform(cropped_img)

               #Transform Image into a tensor for std and mean
            resized_tensor_crop = transform_to_tensor(resized_img_crop)
            resized_tensor_aug = transform_to_tensor(resized_img_aug)


            name_crop = 'carimg_' + str(idx) + '.jpeg'
            name_aug = 'carimg_' + str(idx+1) + '.jpeg'
            dest_crop = os.path.join(save_path, name_crop)
            dest_aug = os.path.join(save_path, name_aug)

                #Save the image
            cv2.imwrite(dest_crop, np.array(resized_img_crop)) #give the names here
            cv2.imwrite(dest_aug, np.array(resized_img_aug)) #give the names here

            idx += 2
            #print(idx)


#The folder where the folder for resized image is
os.chdir(os.path.dirname(save_path))
input_folder = os.path.dirname(save_path)
#input_folder = 'C:/Users/Basu/Documents/input_folder'

####################    Stratification     #############################################################################

# make folders for dent, scratch, rim, other in input folder
# example path = C:/Users/Basu/Documents/input_folder/dent
json_file2 = open("C:/Users/Basu/Documents/AMI/annotations_label.json", "r", encoding="utf-8")
json_data2 = json.load(json_file2)        #loaded the labelled annotations file
import shutil
for anno in json_data2['annotations']:      #loop through each file
    if anno["filename"][0] == 'n':          #ignore normalised files, just work on resized augmented images for stratification purpose
        continue                            # reason: annotations file has two copies of each file: non-normalised and normalised

    if anno["label"]=="dent":
        src= save_path + "/" + anno["filename"]
        dst= "C:/Users/Basu/Documents/input_folder/Dent/dent" + "/" + anno["filename"]
        shutil.copyfile(src,dst)            #takes dents into 'Dent' folder under 'input_folder'
    elif anno["label"]=="scratch":
        src= save_path +  "/" + anno["filename"]
        dst= "C:/Users/Basu/Documents/input_folder/Scratch/scratch" + "/" + anno["filename"]
        shutil.copyfile(src,dst)            #takes scratches into 'Scratch' folder under 'input_folder'
    elif anno["label"]=="rim":
        src= save_path +  "/" + anno["filename"]
        dst= "C:/Users/Basu/Documents/input_folder/Rim/rim" + "/" + anno["filename"]
        shutil.copyfile(src,dst)            #takes rims into 'Rim' folder under 'input_folder'
    elif anno["label"]=="other":
        src= save_path +  "/" + anno["filename"]
        dst= "C:/Users/Basu/Documents/input_folder/Other/other" + "/" + anno["filename"]
        shutil.copyfile(src,dst)            #takes others into 'Other' folder under 'input_folder'

# apply splitfolder ratio to each damage type folder
# store the 3 new folders inside another damage folder under 'output'
splitfolders.ratio('C:/Users/Basu/Documents/input_folder/Dent',output='C:/Users/Basu/Documents/output/dent',seed=42,ratio=(0.8,0.1,0.1))
splitfolders.ratio('C:/Users/Basu/Documents/input_folder/Scratch',output='C:/Users/Basu/Documents/output/scratch',seed=42,ratio=(0.8,0.1,0.1))
splitfolders.ratio('C:/Users/Basu/Documents/input_folder/Rim',output='C:/Users/Basu/Documents/output/rim',seed=42,ratio=(0.8,0.1,0.1))
splitfolders.ratio('C:/Users/Basu/Documents/input_folder/Other',output='C:/Users/Basu/Documents/output/other',seed=42,ratio=(0.8,0.1,0.1))

# now we have train,test,val folders for each kind of damage
# merge all training folders into one final train folder, same for test and val - manually done

###########################################################################################################################################



###########################     Normalisation        ###################################################################################

#take the train dataset into the variable 'train_dataset'
#change the file path below to the manually created combined train folder
train_dataset = torchvision.datasets.ImageFolder(root='C:/Users/Basu/Documents/output/train',transform = transform_to_tensor )
train_loader = DataLoader(train_dataset, batch_size = 32, shuffle = False, num_workers = 0)

# calculate mean and std on train dataset
def get_mean_std(loader):
    images,_ = next(iter(loader))
    mean,std = images.mean([0,2,3]), images.std([0,2,3])
    return mean,std

mean,std = get_mean_std(train_loader)
print('mean and std: \n',mean,std)

normalize_transform = T.Normalize(mean, std)

#apply same normalisation parameters to test set
test_path = 'C:/Users/Basu/Documents/output/test/Aug_crop_resized_images' #The path to the folder having test images
test_images = os.listdir(test_path)
for test_img_file in test_images:
    source = os.path.join(test_path,test_img_file)
    test_img = cv2.imread(source)
    #Transform the Image into a tensor
    test_img_tensor = transform_to_tensor(test_img)
    #Normalize the Image
    test_tensor_norm = normalize_transform(test_img_tensor)
    #Transform it back into an Image
    test_img_norm = transform_PIL(test_tensor_norm)
    #Name the Image File
    test_name_norm = 'norm_' + test_img_file
    test_dest_norm = os.path.join(save_path_norm_test, test_name_norm)
    #Save the Image
    cv2.imwrite(test_dest_norm, np.array(test_img_norm))


#apply same normalisation parameters to train set  
train_path = 'C:/Users/Basu/Documents/output/train/Aug_crop_resized_images' #The path to the folder containing train images
train_images = os.listdir(train_path)
for train_img_file in train_images:
    source = os.path.join(train_path,train_img_file)
    train_img = cv2.imread(source)
    #Transform the Image into a tensor
    train_img_tensor = transform_to_tensor(train_img)
    #Normalize the Image
    train_tensor_norm = normalize_transform(train_img_tensor)
    #Transform it back into an Image
    train_img_norm = transform_PIL(train_tensor_norm)
    #Name the Image File
    train_name_norm = 'norm_' + train_img_file
    train_dest_norm = os.path.join(save_path_norm_train, train_name_norm)
    #Save the Image
    cv2.imwrite(train_dest_norm, np.array(train_img_norm))


#apply same normalisation parameters to validation set 
val_path = 'C:/Users/Basu/Documents/output/val/Aug_crop_resized_images' # The path to the folder containing validation images
val_images = os.listdir(val_path)
for val_img_file in val_images:
    source = os.path.join(val_path,val_img_file)
    val_img = cv2.imread(source)
    #Transform the Image into a tensor
    val_img_tensor = transform_to_tensor(val_img)
    #Normalize the Image
    val_tensor_norm = normalize_transform(val_img_tensor)
    #Transform it back into an Image
    val_img_norm = transform_PIL(val_tensor_norm)
    #Name the Image File
    val_name_norm = 'norm_' + val_img_file
    val_dest_norm = os.path.join(save_path_norm_val, val_name_norm)
    #Save the Image
    cv2.imwrite(val_dest_norm, np.array(val_img_norm))

