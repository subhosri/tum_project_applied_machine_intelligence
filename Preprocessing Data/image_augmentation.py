#import libraries

from PIL import Image
import torchvision.transforms as transforms
import json
import cv2
import os.path
import torchvision.transforms as T
import numpy as np



json_file = open("C:/Users/SUBHOSRI/OneDrive/Documents/AMI/Annotations/annotated_functional_test3_fixed.json", "r", encoding="utf-8")
json_data = json.load(json_file)

#Path with the original images
abs_path = "C:/Users/SUBHOSRI/OneDrive/Documents/AMI/Images"
#Path for the augmented images
save_path = "C:/Users/SUBHOSRI/OneDrive/Documents/AMI/Augmented Images"


idx = 1 #Image Index
for i in json_data['annotations']:
     for j in json_data["images"]:
            if i["image_id"] == j["id"]:
                
                # path to the original image
                source = os.path.join(abs_path, j['file_name'])
                
                #load image
                img = Image.open(source)
                
                # image transformation
                dataset_transform = transforms.Compose([transforms.RandomHorizontalFlip(),transforms.RandomRotation(180),transforms.ToTensor()])
                image = dataset_transform(img)
                transform = T.ToPILImage()
                img = transform(image)
               

                name = 'aug_carimg_' + str(idx) + '.jpeg'
                dest = os.path.join(save_path, name)

                
                #Save the image
                cv2.imwrite(dest, np.array(img))

                idx += 1
