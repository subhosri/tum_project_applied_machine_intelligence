'''
based on: "extract_bbox.py", "imagecrop.py" and "Image_augmentation.py" 

This skript will take as input the augmented, croped and resized. They are split into test, train and val folders.
The resized images in test, train and val folders are  then normalized and stored in the norm_test, norm_train and 
norm_val folders respectively. Then the labels corresponding to norm_test, norm_train and norm_ val are one hot encoded.

The names of the images are as follows:
   - Cropped + Augumented + Resized Images: "carimg_index.jpeg"
   - Normalized Images : "norm_carimg_index.jpeg"

'''


import torch
import torchvision
import torchvision.transforms as T
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
import json
import cv2
import os.path
import splitfolders
import numpy as np


transform_to_tensor = T.ToTensor()
transform_PIL = T.ToPILImage()
resized_transform = T.Resize((224,224))

#input_folder is the folder which contains the Aug_crop_resized_images folder
input_folder = 'C:/Users/Basu/Documents/input_folder'
#Path for the normalised test images
save_path_norm_test = "C:/Users/Basu/Documents/AMI/norm_test"
#Path for the normalised train images
save_path_norm_train = "C:/Users/Basu/Documents/AMI/norm_train"
#Path for the normalised validation images
save_path_norm_val = "C:/Users/Basu/Documents/AMI/norm_val"
#Path for the test images
test_path = 'C:/Users/Basu/Documents/output/test/Aug_crop_resized_images'
#Path for the train images
train_path = 'C:/Users/Basu/Documents/output/train/Aug_crop_resized_images'
#Path for the validation images
val_path = 'C:/Users/Basu/Documents/output/val/Aug_crop_resized_images'
#json_file have tha annotations (labels: dent,sratch,rim,other)
json_file = open("C:/Users/Basu/Documents/AMI/annotations_label.json", "r", encoding="utf-8")
json_data = json.load(json_file)




#The images inside the Aug_crop_resized_images folder is split into test,train and validation folders
#and they are stored inside the folder named output
splitfolders.ratio(input_folder,output='C:/Users/Basu/Documents/output',seed=42,ratio=(0.8,0.1,0.1))

train_dataset = torchvision.datasets.ImageFolder(root='C:/Users/Basu/Documents/output/train',transform = transform_to_tensor )
train_loader = DataLoader(train_dataset, batch_size = 32, shuffle = False, num_workers = 0)

#function calculating the mean and std for the train dataset
def get_mean_std(loader):
    images,_ = next(iter(loader))
    mean,std = images.mean([0,2,3]), images.std([0,2,3])
    return mean,std

mean,std = get_mean_std(train_loader)
#print('mean and std: \n',mean,std)

normalize_transform = T.Normalize(mean, std)

#The path to the folder having test images

#test_path = 'C:/Users/Basu/Documents/output/test/Aug_crop_resized_images'
test_images = os.listdir(test_path)
for test_img_file in test_images:
    source = os.path.join(test_path,test_img_file)
    test_img = cv2.imread(source)
    #Transform the Image into a tensor
    test_img_tensor = transform_to_tensor(test_img)
    #Normalize the Image
    test_tensor_norm = normalize_transform(test_img_tensor)
    #Transform it back into an Image
    test_img_norm = transform_PIL(test_tensor_norm)
    #Name the Image File
    test_name_norm = 'norm_' + test_img_file
    test_dest_norm = os.path.join(save_path_norm_test, test_name_norm)
    #Save the Image
    cv2.imwrite(test_dest_norm, np.array(test_img_norm))

#The path to the folder containing train images
  
#train_path = 'C:/Users/Basu/Documents/output/train/Aug_crop_resized_images'
train_images = os.listdir(train_path)
for train_img_file in train_images:
    source = os.path.join(train_path,train_img_file)
    train_img = cv2.imread(source)
    #Transform the Image into a tensor
    train_img_tensor = transform_to_tensor(train_img)
    #Normalize the Image
    train_tensor_norm = normalize_transform(train_img_tensor)
    #Transform it back into an Image
    train_img_norm = transform_PIL(train_tensor_norm)
    #Name the Image File
    train_name_norm = 'norm_' + train_img_file
    train_dest_norm = os.path.join(save_path_norm_train, train_name_norm)
    #Save the Image
    cv2.imwrite(train_dest_norm, np.array(train_img_norm))


# The path to the folder containing validation images
#val_path = 'C:/Users/Basu/Documents/output/val/Aug_crop_resized_images'
val_images = os.listdir(val_path)
for val_img_file in val_images:
    source = os.path.join(val_path,val_img_file)
    val_img = cv2.imread(source)
    #Transform the Image into a tensor
    val_img_tensor = transform_to_tensor(val_img)
    #Normalize the Image
    val_tensor_norm = normalize_transform(val_img_tensor)
    #Transform it back into an Image
    val_img_norm = transform_PIL(val_tensor_norm)
    #Name the Image File
    val_name_norm = 'norm_' + val_img_file
    val_dest_norm = os.path.join(save_path_norm_val, val_name_norm)
    #Save the Image
    cv2.imwrite(val_dest_norm, np.array(val_img_norm))
    
    
#### One hot encoding
norm_train_images = []
norm_train_labels = []
norm_test_images = []
norm_test_labels = []
norm_val_images = []
norm_val_labels = []

norm_train_name = os.listdir(save_path_norm_train)
for name in norm_train_name:
    for anno in json_data['annotations']:
        if anno["filename"] == name:
            img = cv2.imread(save_path_norm_train + "/" + name)
            norm_train_images.append(img)
            if anno["label"]=="dent":
                norm_train_labels.append(0)
                norm_train_labels.append(0)
                norm_train_labels.append(0)
                norm_train_labels.append(1)
            if anno["label"]=="scratch":
                norm_train_labels.append(0)
                norm_train_labels.append(0)
                norm_train_labels.append(1)
                norm_train_labels.append(0)
            if anno["label"]=="rim":
                norm_train_labels.append(0)
                norm_train_labels.append(1)
                norm_train_labels.append(0)
                norm_train_labels.append(0)
            if anno["label"]=="other":
                norm_train_labels.append(1)
                norm_train_labels.append(0)
                norm_train_labels.append(0)
                norm_train_labels.append(0)

#one hot encoded labels for training images
norm_train_images= np.array(norm_train_images)
norm_train_labels= np.array(norm_train_labels)
#print(norm_train_labels)

norm_test_name = os.listdir(save_path_norm_test)
for name in norm_test_name:
    for anno in json_data['annotations']:
        if anno["filename"] == name:
            img = cv2.imread(save_path_norm_test + "/" + name)
            norm_test_images.append(img)
            if anno["label"]=="dent":
                norm_test_labels.append(0)
                norm_test_labels.append(0)
                norm_test_labels.append(0)
                norm_test_labels.append(1)
            if anno["label"]=="scratch":
                norm_test_labels.append(0)
                norm_test_labels.append(0)
                norm_test_labels.append(1)
                norm_test_labels.append(0)
            if anno["label"]=="rim":
                norm_test_labels.append(0)
                norm_test_labels.append(1)
                norm_test_labels.append(0)
                norm_test_labels.append(0)
            if anno["label"]=="other":
                norm_test_labels.append(1)
                norm_test_labels.append(0)
                norm_test_labels.append(0)
                norm_test_labels.append(0)

#one hot encoded labels for test images                
norm_test_images= np.array(norm_test_images)
norm_test_labels= np.array(norm_test_labels)
#print(norm_test_labels)



norm_val_name = os.listdir(save_path_norm_val)
for name in norm_val_name:
    for anno in json_data['annotations']:
        if anno["filename"] == name:
            img = cv2.imread(save_path_norm_val + "/" + name)
            norm_val_images.append(img)
            if anno["label"]=="dent":
                norm_val_labels.append(0)
                norm_val_labels.append(0)
                norm_val_labels.append(0)
                norm_val_labels.append(1)
            if anno["label"]=="scratch":
                norm_val_labels.append(0)
                norm_val_labels.append(0)
                norm_val_labels.append(1)
                norm_val_labels.append(0)
            if anno["label"]=="rim":
                norm_val_labels.append(0)
                norm_val_labels.append(1)
                norm_val_labels.append(0)
                norm_val_labels.append(0)
            if anno["label"]=="other":
                norm_val_labels.append(1)
                norm_val_labels.append(0)
                norm_val_labels.append(0)
                norm_val_labels.append(0)
#one hot encoded labels for validation images                 
norm_val_images= np.array(norm_val_images)
norm_val_labels= np.array(norm_val_labels)
#print(norm_val_labels)
