import json
import os.path
import shutil
import torch
import torchvision
from torchvision.io import read_image
from torchvision.utils import draw_bounding_boxes
from PIL import Image



json_file = open("C:\\Users\\ereno\\Desktop\\Data\\Annotations\\annotated_functional_test3_fixed.json", "r", encoding="utf-8")
json_data = json.load(json_file)
#my_json_dict = json.loads(json_data)

abs_path = "C:\\Users\\ereno\\Desktop\\Data\\Images"
save_path = 'C:\\Users\\ereno\\Desktop\\damaged_img_bbox'

c=1


for i in json_data['annotations']:
     for j in json_data["images"]:
         if i["image_id"] == j["id"]:

             source = os.path.join(abs_path, j['file_name'])

             try:
                img = read_image(source)


                name = 'img' + str(c) + '.jpeg'

                # x = ((i['bbox'][0])/1920)
                # y = ((i['bbox'][1])/1200)
                # w = ((i['bbox'][2])/1920)
                # h = ((i['bbox'][3])/1200)
                #
                # x_min = (x - round(w/2))
                # x_max = (x + round(w/2))
                # y_min = (y - round(h / 2))
                # y_max = (y + round(h / 2))


                x_min = i['bbox'][0]
                y_min = i['bbox'][1]
                x_max = i['bbox'][0] + i['bbox'][2]
                y_max = i['bbox'][1] + i['bbox'][3]


                bbox = [x_min, y_min, x_max, y_max]
                bbox = torch.tensor(bbox, dtype=torch.int)
                print(bbox)
                print(bbox.size())
                bbox = bbox.unsqueeze(0)
                print(bbox.size())


                # draw bounding box on the input image
                img=draw_bounding_boxes(img, bbox, width=3, colors=(255,255,0))

                name = 'img' + str(c) + '.jpeg'

                # transform it to PIL image and display
                img = torchvision.transforms.ToPILImage()(img)
                # img.show()

                #source = os.path.join(abs_path, j['file_name'])



                dest = os.path.join(save_path, name)

                img.save(dest)

                c += 1


             except:
                 pass


  
