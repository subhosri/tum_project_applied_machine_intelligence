'''

This skript crops down images to the coordinates specified in the .json file.

based on: "extract_bounding_box.py" 
'''

import json
import cv2
import os.path

json_file = open("/Users/Basu/Documents/Programming/AMI_Project/Data/Annotations/annotated_functional_test3_fixed.json", "r", encoding="utf-8")
json_data = json.load(json_file)

#Path with the original images
abs_path = "/Users/Basu/Documents/Programming/AMI_Project/Data/Images"
#Path for the croped images
save_path = "/Users/Basu/Documents/Programming/AMI_Project/Crop_Images"

idx = 1 #Image Index
for i in json_data['annotations']:
     for j in json_data["images"]:
         if i["image_id"] == j["id"]:
            
            #Image Source
             source = os.path.join(abs_path, j['file_name'])

             try:
                
                #Load Image
                img = cv2.imread(source)

                #Box coordinats
                x_min = i['bbox'][0]
                y_min = i['bbox'][1]
                x_max = i['bbox'][0] + i['bbox'][2]
                y_max = i['bbox'][1] + i['bbox'][3]

                #Image Crop
                crop_img = img[y_min:y_max, x_min:x_max]

                name = 'carimg_' + str(idx) + '.jpeg'
                dest = os.path.join(save_path, name)

                #Save the image
                cv2.imwrite(dest, crop_img)

                idx += 1
             except:

                print("MyError: 1")
